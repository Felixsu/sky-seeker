package com.felixsu.skyseeker.ui.fragment;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.felixsu.skyseeker.R;
import com.felixsu.skyseeker.constant.Constants;
import com.felixsu.skyseeker.constant.LogConstants;
import com.felixsu.skyseeker.listener.ActivityCallbackListener;
import com.felixsu.skyseeker.model.ForecastWrapper;
import com.felixsu.skyseeker.model.forecast.Forecast;
import com.felixsu.skyseeker.model.forecast.Hourly;
import com.felixsu.skyseeker.model.formatter.ChartLeftYAxisFormatter;
import com.felixsu.skyseeker.model.formatter.ChartRightYAxisFormatter;
import com.felixsu.skyseeker.model.formatter.ChartXAxisFormatter;
import com.felixsu.skyseeker.model.formatter.PercentageValueFormatter;
import com.felixsu.skyseeker.model.request.ForecastRequest;
import com.felixsu.skyseeker.util.ForecastUtil;
import com.felixsu.skyseeker.util.Util;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ForecastFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = ForecastFragment.class.getName();

    private TextView mDayLabel;
    private TextClock mTimeLabel;
    private TextView mUpdatedAtLabel;
    private ImageView mToggleDetailButton;
    private View mLightLoadingProgress;
    private ImageView mWeatherIcon;
    private TextView mLocationNameLabel;
    private TextView mTemperatureLabel;
    private TextView mApparentTemperatureLabel;
    private TextView mWeatherDetailLabel;
    private TextView mWindDirectionLabel;
    private TextView mWindSpeedLabel;
    private TextView mHumidityLabel;
    private TextView mPrecipChanceLabel;
    private LineChart mChart;
    private ExpandableRelativeLayout mExpandableLayout;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    //data
    private ForecastWrapper mForecastWrapper;
    private Forecast mForecast;

    //state
    private boolean mLightLoading = false;
    private boolean mHeavyLoading = false;
    private boolean mChartShown = true;//should in sync with expandable layout state;

    private ActivityCallbackListener mListener;

    public ForecastFragment() {
    }

    public static ForecastFragment newInstance(ForecastWrapper forecastWrapper) {
        ForecastFragment fragment = new ForecastFragment();
        Bundle args = new Bundle();

        if (forecastWrapper != null) {
            args.putSerializable(Constants.BUNDLE_FORECAST, forecastWrapper);
        }

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "pressed " + view.getId());
        switch (view.getId()) {
            case R.id.button_toggleDetail:
                toggleDetailView();
                break;
            default:
                Log.w(TAG, "clicking unregistered object");
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initForecastData(getArguments());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        initView(rootView);
        configChart();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        updateViewValue();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActivityCallbackListener) {
            mListener = (ActivityCallbackListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ActivityCallbackListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        Log.d(TAG, "start refreshing update");
        updateWeather();
    }

    private void initForecastData(Bundle bundle){
        if (bundle.containsKey(Constants.BUNDLE_FORECAST)){
            mForecastWrapper = (ForecastWrapper) bundle.getSerializable(Constants.BUNDLE_FORECAST);
            if (mForecastWrapper == null) {
                Toast.makeText(getActivity(), "something goes wrong, contact our support", Toast.LENGTH_SHORT).show();
                throw new RuntimeException("Forecast wrapper null");
            }

            mForecast = mForecastWrapper.getForecast();
            new CountDownTimer(3000, 1000) {
                public void onTick(long millisUntilFinished) {}
                public void onFinish() {updateWeather();}
            }.start();
        }
    }

    private void updateWeather(){
        Log.d(TAG, "entering update weather");
        if (mListener != null) {
            Log.d(TAG, "weather update listener not null");
            if (mForecastWrapper.isPrimary()) {
                Location location = mListener.getLocation();
                if (location == null) {
                    Log.w(TAG, "updated weather failed, location is null");
                    return;
                }
                double longitude = location.getLongitude();
                double latitude = location.getLatitude();
                Log.i(TAG, "requesting forecast for " + latitude + " " + longitude);
                mListener.onRequestForecast(new ForecastRequest(latitude, longitude, ForecastRequest.SI_UNIT), mForecastWrapper.getUuid());
            } else {
                double longitude = mForecastWrapper.getLongitude();
                double latitude = mForecastWrapper.getLatitude();
                mListener.onRequestForecast(new ForecastRequest(latitude, longitude, ForecastRequest.SI_UNIT), mForecastWrapper.getUuid());
            }
        }
    }

    private void initView(View rootView){
        mUpdatedAtLabel = (TextView) rootView.findViewById(R.id.label_updated_at);
        mDayLabel = (TextView) rootView.findViewById(R.id.label_day);
        mTimeLabel = (TextClock) rootView.findViewById(R.id.label_time);
        mLightLoadingProgress = rootView.findViewById(R.id.progress_light_loading);
        mWeatherIcon = (ImageView) rootView.findViewById(R.id.ic_weather);
        mLocationNameLabel = (TextView) rootView.findViewById(R.id.label_location_name);
        mTemperatureLabel = (TextView) rootView.findViewById(R.id.label_temperature);
        mApparentTemperatureLabel = (TextView) rootView.findViewById(R.id.label_apparentTemperature);
        mWeatherDetailLabel = (TextView) rootView.findViewById(R.id.label_weatherDetail);
        mWindDirectionLabel = (TextView) rootView.findViewById(R.id.label_windDirection);
        mWindSpeedLabel = (TextView) rootView.findViewById(R.id.label_windSpeed);
        mHumidityLabel = (TextView) rootView.findViewById(R.id.label_humidity);
        mPrecipChanceLabel = (TextView) rootView.findViewById(R.id.label_precipChance);
        mChart = (LineChart) rootView.findViewById(R.id.chart_line);

        mToggleDetailButton = (ImageView) rootView.findViewById(R.id.button_toggleDetail);

        mExpandableLayout = (ExpandableRelativeLayout) rootView.findViewById(R.id.expandable_detail);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.layout_swipeRefresh);

        mToggleDetailButton.setOnClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorRed),
                getResources().getColor(R.color.colorYellow),
                getResources().getColor(R.color.colorBlue),
                getResources().getColor(R.color.colorGreen));

        mTimeLabel.setFormat12Hour("hh:mm a");
        mTimeLabel.setFormat24Hour("hh:mm a");
    }

    private void updateViewValue(){
        //todo not complete yet
        Log.i(TAG, "i am " + getTag());
        final String temperatureCelsiusText = "%d " + "°C";
        final String feelsLikeCelsiusText = "Feels like %d " + "°C";
        final String windSpeedText = "%d " + "m/s";
        final String humidityText = "%d " + "%%";
        final String precipChanceText = "%d " + "%%";

        mDayLabel.setText(Util.getDayName(new Date()));
        if (mForecastWrapper != null) {
            long updateTime = mForecastWrapper.getUpdatedAt();
            if (updateTime == 0) {
                updateTime = new Date().getTime();
            }
            mUpdatedAtLabel.setText("Last updated: " + Util.getVerboseDate(new Date(updateTime)));

            if (mForecastWrapper.getSubAdministrativeLocation() == null && mForecastWrapper.getAdministrativeLocation() == null){
                mLocationNameLabel.setText("Loading");
                mLightLoadingProgress.setVisibility(View.VISIBLE);
            } else {
                Log.i(TAG, "subLocation: " + mForecastWrapper.getSubAdministrativeLocation() + " admin: " + mForecastWrapper.getAdministrativeLocation());
                String location = mForecastWrapper.getSubAdministrativeLocation() != null ? mForecastWrapper.getSubAdministrativeLocation() : mForecastWrapper.getAdministrativeLocation();
                mLocationNameLabel.setText(location + ", " + mForecastWrapper.getCountry());
                mLightLoadingProgress.setVisibility(View.GONE);
            }
        } else {
            mLocationNameLabel.setText("Not Available");
        }

        if (mForecast != null){
            mWeatherIcon.setImageResource(ForecastUtil.getIcon(mForecast.getCurrently().getIcon()));
            mTemperatureLabel.setText(String.format(temperatureCelsiusText, mForecast.getCurrently().getTemperature().intValue()));
            mApparentTemperatureLabel.setText(String.format(feelsLikeCelsiusText, mForecast.getCurrently().getApparentTemperature().intValue()));
            mWeatherDetailLabel.setText(mForecast.getHourly().getSummary());
            mWindDirectionLabel.setText(ForecastUtil.directionValue(mForecast.getCurrently().getWindBearing()));
            mWindSpeedLabel.setText(String.format(windSpeedText, mForecast.getCurrently().getWindSpeed().intValue()));
            mHumidityLabel.setText(String.format(humidityText, Util.percentValue(mForecast.getCurrently().getHumidity())));
            mPrecipChanceLabel.setText(String.format(precipChanceText, Util.percentValue(mForecast.getCurrently().getPrecipProbability())));
            fillChart();
        } else {
            mWeatherIcon.setImageResource(R.drawable.ic_weather_sunny);
            mTemperatureLabel.setText("-");
            mApparentTemperatureLabel.setText("Feels like -");
            mWeatherDetailLabel.setText("please refresh");
            mWindDirectionLabel.setText("-");
            mWindSpeedLabel.setText("-");
            mHumidityLabel.setText("-");
            mPrecipChanceLabel.setText("-");
        }
    }

    public void onForecastUpdated(Bundle bundle) {
        Log.d(TAG, "on weather updated");
        int resultCode = bundle.getInt(Constants.RESULT_CODE_KEY);
        if (resultCode == Constants.RETURN_OK) {
            mForecastWrapper = (ForecastWrapper) bundle.getSerializable(Constants.BUNDLE_FORECAST);
            if (mForecastWrapper == null) {
                Log.e(TAG, LogConstants.UNEXPECTED_ERROR);
                throw new RuntimeException(LogConstants.UNEXPECTED_ERROR);
            }
            mForecast = mForecastWrapper.getForecast();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateViewValue();
                }
            });
        } else {
            Log.w(TAG, "weather update error");
        }

        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void toggleDetailView() {
        if (mChartShown){
            mExpandableLayout.collapse(400, new DecelerateInterpolator());
            mChartShown = false;
            mToggleDetailButton.setImageResource(R.drawable.ic_down_arrow);
        } else {
            mExpandableLayout.expand(400, new DecelerateInterpolator());
            mChartShown = true;
            mToggleDetailButton.setImageResource(R.drawable.ic_up_arrow);
        }

    }

    private void fillChart(){
        final int FORECAST_HOUR = 8;

        final Hourly hourly = mForecast.getHourly();
        final List<Hourly.Data> hData = hourly.getData();

        List<Entry> temperatureFc = new ArrayList<>();
        List<Entry> apparentTemperatureFc = new ArrayList<>();
        List<Entry> precipChanceFc = new ArrayList<>();

        double minTemp = 100;
        double maxTemp = 0;

        for (int i = 0; i < FORECAST_HOUR; i++){
            double currentTemp = hData.get(i).getTemperature();
            temperatureFc.add(new Entry((float) i, (float) currentTemp));
            maxTemp = Util.max(currentTemp, maxTemp);
            minTemp = Util.min(currentTemp, minTemp);

            double apparentTemp = hData.get(i).getApparentTemperature();
            apparentTemperatureFc.add(new Entry((float) i, (float) apparentTemp));
            maxTemp = Util.max(apparentTemp, maxTemp);
            minTemp = Util.min(apparentTemp, minTemp);

            precipChanceFc.add(new Entry((float) i, 100f*(hData.get(i).getPrecipProbability().floatValue())));
        }

        adjustLeftAxisRange((float) minTemp, (float) maxTemp);

        //actual temperature
        LineDataSet temperatureFcDataSet = new LineDataSet(temperatureFc, "Current Temperature");
        temperatureFcDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        temperatureFcDataSet.setDrawCircles(false);
        temperatureFcDataSet.setLineWidth(2f);
        temperatureFcDataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        temperatureFcDataSet.setColor(getContext().getResources().getColor(R.color.colorPrimaryDark));
        temperatureFcDataSet.setFillColor(getContext().getResources().getColor(R.color.colorPrimaryDark));
        temperatureFcDataSet.setFillAlpha(0x80);
        temperatureFcDataSet.setDrawFilled(true);
        temperatureFcDataSet.setDrawValues(false);

        //apparent temperature
        LineDataSet apparentTemperatureFcDataSet = new LineDataSet(apparentTemperatureFc, "Apparent Temperature");
        apparentTemperatureFcDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        apparentTemperatureFcDataSet.setDrawCircles(false);
        apparentTemperatureFcDataSet.setLineWidth(2f);
        apparentTemperatureFcDataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        apparentTemperatureFcDataSet.setColor(getContext().getResources().getColor(R.color.colorPrimary));
        apparentTemperatureFcDataSet.setFillColor(getContext().getResources().getColor(R.color.colorPrimary));
        apparentTemperatureFcDataSet.setFillAlpha(0x80);
        apparentTemperatureFcDataSet.setDrawFilled(true);
        apparentTemperatureFcDataSet.setDrawValues(false);

        //precip probability
        LineDataSet precipChanceFcDataSet = new LineDataSet(precipChanceFc, "Precip Probability");
        precipChanceFcDataSet.setAxisDependency(YAxis.AxisDependency.RIGHT);
        precipChanceFcDataSet.setMode(LineDataSet.Mode.LINEAR);
        precipChanceFcDataSet.setColor(getContext().getResources().getColor(R.color.colorAccent));
        precipChanceFcDataSet.setCircleColor(getContext().getResources().getColor(R.color.colorAccent));
        precipChanceFcDataSet.setLineWidth(2f);
        precipChanceFcDataSet.setValueFormatter(new PercentageValueFormatter());
        precipChanceFcDataSet.setValueTextSize(10f);

        mChart.setData(new LineData(temperatureFcDataSet, apparentTemperatureFcDataSet, precipChanceFcDataSet));
        mChart.animateX(400, Easing.EasingOption.EaseOutSine);


    }

    private void adjustLeftAxisRange(float min, float max){
        YAxis yAxisLeft = mChart.getAxisLeft();
        yAxisLeft.setAxisMinValue(min);
        yAxisLeft.setAxisMaxValue(max + 1f);
    }

    private void configChart(){
        Log.i(TAG, "start configuring chart");
        XAxis xAxis = mChart.getXAxis();
        xAxis.setValueFormatter(new ChartXAxisFormatter());
        xAxis.setAxisMinValue((float) ChartXAxisFormatter.MIN_RANGE);
        xAxis.setAxisMaxValue((float) ChartXAxisFormatter.MAX_RANGE);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

        YAxis yAxisLeft = mChart.getAxisLeft();
        yAxisLeft.setValueFormatter(new ChartLeftYAxisFormatter());
        yAxisLeft.setLabelCount(5, true);
        yAxisLeft.setDrawGridLines(true);
        yAxisLeft.setDrawAxisLine(false);

        YAxis yAxisRight = mChart.getAxisRight();
        yAxisRight.setValueFormatter(new ChartRightYAxisFormatter());
        yAxisRight.setLabelCount(4, true);
        yAxisRight.setAxisMinValue(0f);
        yAxisRight.setAxisMaxValue(100f);
        yAxisRight.setDrawGridLines(false);
        yAxisRight.setDrawAxisLine(false);
        yAxisRight.setDrawLabels(false);

        mChart.getLegend().setPosition(Legend.LegendPosition.ABOVE_CHART_LEFT);
        mChart.setScaleEnabled(false);
        mChart.setPinchZoom(false);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setHighlightPerDragEnabled(false);
        mChart.setHighlightPerTapEnabled(false);
        mChart.setGridBackgroundColor(getContext().getResources().getColor(R.color.colorWhite));
        mChart.setDrawGridBackground(false);
        mChart.setDescription("");


    }

}
