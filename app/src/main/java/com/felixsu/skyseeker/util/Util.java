package com.felixsu.skyseeker.util;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.felixsu.skyseeker.BuildConfig;
import com.felixsu.skyseeker.R;
import com.felixsu.skyseeker.model.TimeState;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    private static final SimpleDateFormat SDF = new SimpleDateFormat();

    public static String base64Encode(byte[] input, int len) {
        return Base64.encodeToString(input, 0, len, Base64.CRLF);
    }

    public static byte[] base64Decode(String input) {
        return Base64.decode(input, Base64.CRLF);
    }


    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static int percentValue(double value) {
        return (int) (value * 100);
    }

    public static String generateGreetings() {
        String result;
        switch (getTimeState()){
            case TIME_MORNING:
                result = "Good Morning";
                break;
            case TIME_AFTERNOON:
                result = "Good Afternoon";
                break;
            case TIME_NIGHT:
                result = "Good Evening";
                break;
            default:
                result = "Good Morning";
                break;
        }
        return result;
    }

    public static int getDrawerBackgroundResource(){
        int result;
        switch (getTimeState()){
            case TIME_MORNING:
                result = R.drawable.material_morning;
                break;
            case TIME_AFTERNOON:
                result = R.drawable.material_afternoon;
                break;
            case TIME_NIGHT:
                result = R.drawable.material_night;
                break;
            default:
                result = R.drawable.material_morning;
                break;
        }
        return result;
    }

    public static TimeState getTimeState(){
        SimpleDateFormat sd = new SimpleDateFormat("HH");
        String hourString = sd.format(new Date());
        int hourInteger = Integer.parseInt(hourString);

        if (hourInteger >= 0 && hourInteger < 10) {
            return TimeState.TIME_MORNING;
        } else if (hourInteger >= 10 && hourInteger < 16) {
            return TimeState.TIME_AFTERNOON;
        } else {
            return TimeState.TIME_NIGHT;
        }
    }

    public static String modifyGooglePictureUrl(String input){
        final String pictureUrl96 = "s96-c";
        final String newPictureUrl = "s256-c";

        if (input != null){
            String result;
            if (input.contains(pictureUrl96)){
                String[] as = input.split(pictureUrl96);
                result = as[0] + newPictureUrl + as[1];
            } else {
                result = input;
            }
            return result;
        } else {
            return null;
        }
    }

    public static long getTime() {
        return new Date().getTime();
    }

    public static String getDayName(Date d){
        SDF.applyPattern("EEE ");
        return SDF.format(d);
    }

    public static String getVerboseDate(Date d){
        SDF.applyPattern("EEE, hh:mm a");
        return SDF.format(d);
    }

    public static String getFirstName(String fullName){
        if (fullName == null || fullName.isEmpty()){
            throw new RuntimeException("name is null");
        }
        String[] t = fullName.split(" ");
        String firstName = t[0];

        return String.valueOf(firstName.charAt(0)).toUpperCase() + firstName.substring(1, firstName.length());
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        //String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                //phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            //phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }

    public static String getAppVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public static String getAndroidVersion() {
        return String.valueOf(Build.VERSION.SDK_INT);
    }

    public static double max(double a, double b){
        return a > b ? a : b;
    }

    public static double min(double a, double b){
        return a < b ? a : b;
    }
}