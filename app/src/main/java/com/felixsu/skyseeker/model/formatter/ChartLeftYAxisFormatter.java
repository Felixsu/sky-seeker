package com.felixsu.skyseeker.model.formatter;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.AxisValueFormatter;

/**
 * Created by felixsoewito on 8/17/16.
 */
public class ChartLeftYAxisFormatter implements AxisValueFormatter {
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        String val = String.format("%.1f", value);
        return val + "°";
    }

    @Override
    public int getDecimalDigits() {
        return 0;
    }
}
