package com.felixsu.skyseeker.model.formatter;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.AxisValueFormatter;

/**
 * Created by felixsoewito on 8/17/16.
 */
public class ChartRightYAxisFormatter implements AxisValueFormatter {
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        int val = Math.round(value);
        return String.valueOf(val) + "%";
    }

    @Override
    public int getDecimalDigits() {
        return 0;
    }
}
