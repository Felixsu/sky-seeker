package com.felixsu.skyseeker.model;

/**
 * Created by felixsoewito on 8/20/16.
 */
public enum TimeState {
    TIME_MORNING,
    TIME_AFTERNOON,
    TIME_NIGHT
}
