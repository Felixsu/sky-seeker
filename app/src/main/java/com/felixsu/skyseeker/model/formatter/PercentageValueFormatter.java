package com.felixsu.skyseeker.model.formatter;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

/**
 * Created by felixsoewito on 8/17/16.
 */
public class PercentageValueFormatter implements ValueFormatter {
    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        int val = Math.round(value);
        return String.valueOf(val) + "%";
    }
}
