package com.felixsu.skyseeker.model.formatter;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.AxisValueFormatter;

/**
 * Created by felixsoewito on 8/17/16.
 */
public class ChartXAxisFormatter implements AxisValueFormatter {

    public static final int MIN_RANGE = 0;
    public static final int MAX_RANGE = 7;
    private final String[] axisValues = {"NOW", "+1", "+2", "+3", "+4", "+5", "+6", "+7"};

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return axisValues[(int) value];
    }

    @Override
    public int getDecimalDigits() {
        return 0;
    }
}
